from flask import Flask, jsonify, request, make_response
import sparknlp

spark = sparknlp.start()
# params =>> gpu=False, spark23=False (start with spark 2.3)
# print("Spark NLP version", sparknlp.version())
# print("Apache Spark version:", spark.version)

from sparknlp.pretrained import PretrainedPipeline, WordEmbeddingsModel, NerDLModel, NerConverter
from sparknlp.base import *
from sparknlp.annotator import *
from pyspark.ml import Pipeline

word_embeddings = WordEmbeddingsModel.pretrained("hebrew_cc_300d", "he") \
   .setInputCols(["document", "token"]) \
   .setOutputCol("embeddings")

ner = NerDLModel.pretrained("hebrewner_cc_300d", "he") \
   .setInputCols(["sentence", "token", "embeddings"]) \
   .setOutputCol("ner")

ner_converter = NerConverter().setInputCols(["sentence", "token", "ner"]).setOutputCol("ner_chunk")
document_assembler = DocumentAssembler().setInputCol("text").setOutputCol("document").setCleanupMode("shrink")
sentence_detector = SentenceDetector().setInputCols(["document"]).setOutputCol("sentences")
tokenizer = Tokenizer().setInputCols(["document"]).setOutputCol("token").fit(spark.createDataFrame([['']]).toDF("text"))

nlp_pipeline = Pipeline(stages=[document_assembler, sentence_detector, tokenizer, word_embeddings, ner, ner_converter])
light_pipeline = LightPipeline(nlp_pipeline.fit(spark.createDataFrame([['']]).toDF("text")))

app = Flask(__name__)
app.config['SECRET_KEY'] = 'mgeliDatvi'

@app.route('/api', methods = ['GET'])
def aappii():
    Doc = request.json['text']
    annotations = light_pipeline.fullAnnotate(Doc)
    l=[]
    prev_ty='O'
    for annotation in annotations[0]['ner']:
        tag=annotation.result[:1]
        ty=''
        if (annotation.result!='O') and (float( annotation.metadata['confidence'])>0.8):
            if annotation.result[-4:]=='PERS':
                ty='TAG_PERSON'
            elif annotation.result[-3:]=='ORG':
                ty='TAG_COMPAMY'
            elif annotation.result[-3:]=='LOC':
                ty='TAG_LOCATION'
            if ty:
                if ty==prev_ty and tag=='I':
                    old_key=list(l[-1].keys())[-1]
                    new_key=old_key + ' ' + annotation.metadata['word']
                    l.pop()
                    l.append({new_key:ty})
                else:
                    l.append({annotation.metadata['word']:ty})
        prev_ty=ty
    return jsonify(l)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080, debug=False)
